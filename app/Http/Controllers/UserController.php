<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    //login
    function login(Request $req)
    {
        $userData = User::where(['email'=>$req->email])->first();
        if(!$userData || !Hash::check($req->password, $userData->password))
        {
            return "Username or password does not match";
        }
        else{
            $req->session()->put('user', $userData);
            return redirect('/');
        }
    }

    //register
    function register(Request $req){
        $user = new User;
        $user->name = $req->name;
        $user->email = $req->email;
        $user->password = Hash::make($req->password);
        $user->save();
        return redirect('/login');
    }
}
