<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    // if the table name and model name do not follow the naming convention then this code is needed to connect both
    // public $table = 'cart';
}
