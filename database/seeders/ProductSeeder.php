<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([
            [
                "name"=>"LG Mobile",
                "price"=>"200",
                "description"=>"4gb ram, 128 gb rom",
                "category"=>"mobile",
                "gallery"=>"https://i.gadgets360cdn.com/products/large/lg-wing-1-800x800-1600148741.jpg"
            ],
            [
                "name"=>"LG Mobile 2",
                "price"=>"150",
                "description"=>"4gb ram, 64 gb rom",
                "category"=>"mobile",
                "gallery"=>"https://www.lg.com/in/images/mobile-phones/md06186738/01_Thumb-350_04092019.jpg"
            ],
            [
                "name"=>"LG TV",
                "price"=>"1200",
                "description"=>"For your living room",
                "category"=>"tv",
                "gallery"=>"https://www.arihantelectronics.org/wp-content/uploads/2020/09/32LM636BPTB.jpg"
            ],
            [
                "name"=>"LG Watch",
                "price"=>"100",
                "description"=>"2gb ram, 4 gb rom",
                "category"=>"watch",
                "gallery"=>"https://www.lg.com/us/images/smart-watches/md05800429/gallery/medium01.jpg"
            ],
        ]);
    }
}
