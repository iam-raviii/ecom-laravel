@extends('master')
@section('content')
<div class="custom-product">
    <div class="col-sm-8">
        <div class="trending-wrapper">
            <h4>Results for products</h4>
            <a class="btn btn-success" href="order_now">Order Now</a>
            <br>
            <br>
            @foreach($products as $item)
            <div class="row searched-item cart-list-divider">
                <div class="col-sm-3">
                    <a href="/detail{{$item->id}}">
                        <img class="trending-image" src="{{$item->gallery}}" alt="cart item">
                    </a>
                </div>
                <div class="col-sm-4">
                    <a href="/detail/{{$item->id}}">
                        <div>
                            <h2>{{$item->name}}</h2>
                            <h5>{{$item->description}}</h5>
                        </div>
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="/remove_cart/{{$item->cart_id}}" class="btn btn-warning">Remove from Cart</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<a class="btn btn-success" href="order_now">Order Now</a>
@endsection