<?php 
use App\Http\Controllers\ProductController;
$total = 0;
if(Session::has('user')){
$total = ProductController::cartitem();
}
?>
  {{-- bt 5 --}}
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">Ecom</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Order</a>
        </li>
      </ul>

      <ul class=" navbar-nav ">

        @if(Session::has('user'))
        <li class="nav-item">
          <a class="nav-link" href="#">{{Session::get('user')['name']}}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Cart</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/logout">Log out</a>
        </li> 
        @else
        <li class="nav-item">
          <a class="nav-link" href="/login">Login</a>
        </li> 
        @endif

      </ul>
    </div>
  </div>
</nav>