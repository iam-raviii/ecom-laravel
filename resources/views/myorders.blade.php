@extends('master')
@section('content')
<div class="custom-product">
    <div class="col-sm-8">
        <div class="trending-wrapper">
            <h4>My Orders</h4>
            <br>
            <br>
            @foreach($orders as $item)
            <div class="row searched-item cart-list-divider">
                <div class="col-sm-3">
                    <a href="/detail{{$item->id}}">
                        <img class="trending-image" src="{{$item->gallery}}" alt="cart item">
                    </a>
                </div>
                <div class="col-sm-4">
                        <div>
                            <h2>{{$item->name}}</h2>
                            <h5>{{$item->description}}</h5>
                            <h5>address: {{$item->address}}</h5>
                            <h5>order status: {{$item->status}}</h5>
                            <h5>payment mode: {{$item->payment_method}}</h5>
                            <h5>payment status: {{$item->payment_status}}</h5>
                        </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection