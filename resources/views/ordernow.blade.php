@extends('master')
@section('content')
<div class="custom-product">
    <div class="col-sm-8">
        <table class="table">
         
            <tbody>
              <tr>
                <td>total</td>
                <td>$ {{$total}}</td>
              </tr>
              <tr>
                <td>tax</td>
                <td>$ 0</td>
              </tr>
              <tr>
                <td>Grand total</td>
                <td>$ {{$total}}</td>
              </tr>
            </tbody>
          </table>
          <div>
            <form action="/orderplace" method="POST">
                @csrf
                <div class="form-group">
                  <textarea name="address" placeholder="enter your address" class="form-control"></textarea>
                </div>
                <div class="form-group">
                  <label for="pwd">Payment Method</label>
                  <br><br>
                  <input type="radio" value="cash" name="payment"><span> online payment</span>
                  <br><br>
                  <input type="radio" value="cash" name="payment"><span> Cash on delivery</span>
                </div><br>
                <button type="submit" class="btn btn-success">Order now</button>
              </form>
          </div>
    </div>
</div>
@endsection